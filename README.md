# SingletonLocatorCSharp
With this simple addon you can easily implement Singleton on your class and keep your code tidy at the same time.

## Usage
Use `SingletonLocator<YourClass>.Instance`
